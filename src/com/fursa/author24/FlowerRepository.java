package com.fursa.author24;

import java.util.List;

//Класс FlowerRepository реализует интерфейс для CRUD операций
public class FlowerRepository implements Repository {
    private List<Flower> flowers;

    public FlowerRepository(List<Flower> flowers) {
        this.flowers = flowers;
    }

    //Вывод всего каталога
    @Override
    public void print() {
        flowers.forEach(flower -> {
            System.out.println(flower.toString());
        });
    }

    //Получение по имени
    @Override
    public Flower getByName(String name) {
        int pos = 0; //Находим позицию в списке, а потом просто возвращаем цветок из списка
        for (int i = 0; i < flowers.size(); i++) {
            if(flowers.get(i).getName().equals(name)) {
                pos = i;
            }
        }

        return flowers.get(pos);
    }

    @Override
    public void add(Flower flower) {
        flowers.add(flower);
    }

    //Получение списка
    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }

    //Удаление по имени
    @Override
    public void removeFlower(String name) {
        flowers.removeIf(f -> f.getName().equals(name));
    }

    //Сколько всего цветов в каталоге
    @Override
    public int size() {
        return flowers.size();
    }
}


