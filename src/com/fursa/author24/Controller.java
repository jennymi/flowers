package com.fursa.author24;

import java.util.List;

/**
 * Класс Controller инкапсулирует работу с FlowerRepository
 * Контроллер некая сущность которая умеет осуществлять основные операции над данными в приложении
 * По аналогии с шаблоном MVC (Model(наша модель Flower) - View(Для упрощения понимания это Main класс) - Controller
 * В конструктор передается зависимость repository
 * Почему лучше передавать в конструктор? Потому - что согласно
 * принципу Dependency Inversion Объект не должен иметь жестких связей
 * все зависимости предоставляются извне
 *
 * Хороший подход
 * public Controller(FlowerRepository repository) {
 *     this.repository = repository;
 * }
 * Плохой подход. Создавать зависимость в этом же классе
 * public Controller(FlowerRepository repository) {
 *     repository = new FlowerRepository();
 * }
 */
public class Controller {
    private FlowerRepository repository;

    public Controller(FlowerRepository repository) {
        this.repository = repository;
    }

    public void print() {
        repository.print();
    } //печать списка

    public List<Flower> getAll() {
        return repository.getFlowers();
    } //получение списка

    public int getSize() { return repository.size(); } //получение размера списка

    public void remove(Flower flower) {
        repository.removeFlower(flower.getName());
    } //Удаление объекта из списка

    public Flower getByName(String name) {
        return repository.getByName(name);
    } //Получение цветка по именеи

    public void add(Flower flower) {
        repository.add(flower);
    } //Добавление нового цветка
}
