package com.fursa.author24;

/**
 * Простой POJO объект содержащий поля,
 * как описано в задании
 * также содержит геттеры и сеттеры для получения этих данных
 */
public class Flower {
    private String species; //Вид
    private String name; //Название
    private int cipher; //Шифр
    private String description; //Описание
    private String terms; //Условия выращивания

    public Flower(String species, String name, int cipher, String description, String terms) {
        this.species = species;
        this.name = name;
        this.cipher = cipher;
        this.description = description;
        this.terms = terms;
    }

    public Flower() { }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCipher() {
        return cipher;
    }

    public void setCipher(int cipher) {
        this.cipher = cipher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "species='" + species + '\'' +
                ", name='" + name + '\'' +
                ", cipher=" + cipher +
                ", description='" + description + '\'' +
                ", terms='" + terms + '\'' +
                '}';
    }
}
