package com.fursa.author24;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static FlowerRepository repository;
    private static List<Flower> flowers;
    private static Controller controller;

    static {
        //Добавляем в коллекцию два вида цветов
        flowers = new ArrayList<>();
        Flower flower1 = new Flower();
        flower1.setName("Абутилон");
        flower1.setCipher(101011); //Используем ранее созданные сеттеры, для инициализации полей объекта
        flower1.setDescription("Вырастает до 15 см в высоту");
        flower1.setSpecies("Мальвовые");
        flower1.setTerms("+10 +30 Градусов цельсия, влажный климат");
        //Добавляем созданный обхект в список
        flowers.add(flower1);

        Flower flower2 = new Flower();
        flower2.setName("Агава");
        flower2.setCipher(99999);
        flower2.setDescription("Вырастает до 25 см в высоту");
        flower2.setSpecies("Агава американская");
        flower2.setTerms("+20 +35 Градусов цельсия, сухой климат");

        flowers.add(flower2);
        repository = new FlowerRepository(flowers); //Инициализируем репозиторий
        controller = new Controller(repository); //Инициализируем контроллер
    }

    public static void main(String[] args) throws IOException {
        System.out.println("[+]Тестируем печать всего каталога");
        //Тестируем вывод всех цветов
        controller.print();
        System.out.println();
        //Тестируем getByName
        System.out.println("[+]Тестируем getByName");
        Flower flower = controller.getByName("Агава");
        System.out.println(flower.toString());
        //Тестируем получение размера
        System.out.println("Текущий размер: " + controller.getSize());
        //Тестируем удаление
        controller.remove(flower);
        System.out.println("Размер после удаления Агавы: " + controller.getSize());
        //Тестируем добавление
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Flower newFlower = new Flower();
        System.out.println("Введите имя: ");
        newFlower.setName(String.valueOf(br.readLine()));
        System.out.println("Введите шифр(цифры от 0-9): ");
        newFlower.setCipher(Integer.parseInt(br.readLine()));
        System.out.println("Введите вид: ");
        newFlower.setSpecies(String.valueOf(br.readLine()));
        System.out.println("Введите описание: ");
        newFlower.setDescription(String.valueOf(br.readLine()));
        System.out.println("Введите условия выращивания: ");
        newFlower.setTerms(String.valueOf(br.readLine()));
        controller.add(newFlower); //Обращаемся к контроллеру, добавляем новый цветок
        System.out.println("После добавления в каталоге стало: " + controller.getSize() + " цветов...");

    }

}
