package com.fursa.author24;

import com.fursa.author24.Flower;

import java.util.List;

/*
    Имеется репозиторий, который умеет работать с каталогом. Делает основные операции
    Вставка, чтение, удаление, получение размера, получение инфы
 */
public interface Repository {

     void print(); //Печать списка

     Flower getByName(String name); //Получение цветка по названию

     List<Flower> getFlowers(); //получение всего каталога

     void removeFlower(String name); //Удаление по названию

     void add(Flower flower); //Добавление

     int size(); //Получения размера списка
}
